package accion.com.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import java.util.*

class ViewModelDataGenerator : ViewModel() {

    val TAG: String = this.javaClass.simpleName

    var randomNumber: String? = null

    fun getNumber(): String? {
        Log.i(TAG, "GET Number")
        if (randomNumber == null) {
            createNumber()
        }
        return randomNumber
    }

    fun createNumber() {
        Log.i(TAG, "Create Number")
        var randomNo: Random = Random()
        randomNumber = ("Number = " + randomNo.nextInt((10 - 1) + 1))
    }

    override fun onCleared() {
        super.onCleared()
        Log.i(TAG, "ViewModel Destroyed")
    }
}