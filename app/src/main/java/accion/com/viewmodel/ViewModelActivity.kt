package accion.com.viewmodel

import accion.com.kotlintutorial.R
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_viewmodel.*

/*
* This class we can Learn
* 1) ViewModel
* 2) Use of viewModel
* 3) LiveData
* 4) LiveData with ViewModel and With out ViewModel
* */
class ViewModelActivity : AppCompatActivity() {
    val TAG: String = this.javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewmodel)

        Log.i(TAG, "Random Number Set")

        /*
        * Without view model
        * */
        //var model: ViewModelDataGenerator = ViewModelDataGenerator()


        /**
         * ViewModelProviders is @deprecated use "ViewModelProvider" in place of this.
         * @deprecated This class should not be directly instantiated
         */
        //var model = ViewModelProviders.of(this).get(ViewModelDataGenerator::class.java)


        /*
        * With ViewModelProviders
        * */
        val model: ViewModelDataGenerator =
            ViewModelProvider(this, ViewModelProviderFactory(ViewModelDataGenerator())).get(
                ViewModelDataGenerator::class.java
            )
        var updateNo: String? = model.getNumber()
        random_number.text = updateNo


        /*
        * Without view model Livedata
        * */
        //var LiveDataModel: ViewModelLiveData = ViewModelLiveData()

        /*
        * This Is LiveData with ViewModel.
        * */
        val LiveDataModel: ViewModelLiveData =
            ViewModelProvider(this, ViewModelProviderFactory(ViewModelLiveData())).get(
                ViewModelLiveData::class.java
            )

        var upDateLiveData: MutableLiveData<String> = LiveDataModel.getNumber()
        upDateLiveData.observe(this, Observer<String> { value ->
            value?.let {
                random_number.text = it
            }
            // Update the UI.
        })

        fetch_new_data.setOnClickListener(View.OnClickListener {
            LiveDataModel.createNumber()
        })
    }
}