package accion.com.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class ViewModelLiveData : ViewModel() {

    var TAG: String = this.javaClass.simpleName
    var randomNumber: MutableLiveData<String> = MutableLiveData()

    fun getNumber(): MutableLiveData<String> {
        Log.i(TAG, "GET Number")
        if (randomNumber == null) {
            randomNumber = MutableLiveData<String>()
            createNumber()
        }
        return randomNumber
    }

    fun createNumber() {
        Log.i(TAG, "Create Number")
        var randomNo: Random = Random()
        //randomNumber = ("Number = " + randomNo.nextInt((10 - 1) + 1))
        randomNumber.value = ("Number = " + randomNo.nextInt((10 - 1) + 1))
    }

    override fun onCleared() {
        super.onCleared()
        Log.i(TAG, "ViewModel Destroyed")
    }
}