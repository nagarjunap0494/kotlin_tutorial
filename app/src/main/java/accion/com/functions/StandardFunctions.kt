package accion.com.functions

fun main(args: Array<String>) {

    //letFunctionExample()
    //runFunctionExample()
    //alsoFunctionExample()
    //applyFunctionExample()
    withFunctionExample()
}

fun letFunctionExample() {
    /*
* let function
* let takes the object it is invoked upon as the parameter and returns the result of the lambda expression.
  Kotlin let is a scoping function wherein the variables declared inside the expression cannot be used outside.
* */
    var str = "Hello World"
    str.let { println("$it!!") }
    println(str)
    var strLength = str.let { "$it function".length }
    println("strLength is $strLength") //prints strLength is 25

    /*
    * Chaining let function
    * As you can see we’ve declared a local variable “i” inside the second let function.
    * Setting the last statement of the let function to i returns the property to the outer property a.
    * */
    var a = 1
    var b = 2

    a = a.let { it + 2 }.let {
        val i = it + b
        i
    }
    println(a) //5

    /*
    * Nesting let function
    * We can set a let expression inside another let expression as shown below.
    * */
    var x = "Name"
    x = x.let { outer ->
        outer.let { inner ->
            println("Inner is $inner and outer is $outer")

        }
        "kotlin tutorial outer blocks"
    }
    println(x)

    /*
    * let is useful to null check as well.
    * */
    var name: String? = "Kotlin let null check"
    name?.let { println(it) } //prints Kotlin let null check
    name = null
    name?.let { println(it) } //nothing happens
}


fun runFunctionExample() {
    /*
* run function
* 1) Kotlin run expression can change the outer property.  we’ve redefined it for the local scope.
* 2) Similar to the let function, the run function also returns the last statement.
* 3) Unlike let, the run function doesn’t support the it keyword.
* */
    var runfunction = "This is outside run function"
    println(runfunction)
    runfunction = run {
        val runfunction = "this is inner run function"
        runfunction
    }
    println(runfunction)

    //Let’s combine the let and run functions together.
    var p : String? = null
    p?.let { println("p is $p") } ?: run { println("p was null. Setting default value to: ")
        p = "Kotlin"}

    println(p)
}

fun alsoFunctionExample(){
    /*
    * The also expression returns the data class object whereas the let expression returns nothing (Unit) as we didn’t specify anything explicitly.
    * */
    data class Person(var name: String, var tutorial : String)
    var person = Person("Anupam", "Kotlin")

    var l = person.let { it.tutorial = "Android" }
    var al = person.also { it.tutorial = "Android" }

    println(l)
    println(al)
    println(person)
}

fun applyFunctionExample(){
    /*
    * 1) Kotlin apply is an extension function on a type. It runs on the object reference (also known as receiver) into the expression and returns the object reference on completion.
    * 2) In apply it isn’t allowed. If the property name of the data class is unique in the function, you can omit this. We should use also only when we don’t want to shadow this.
    *  */
    data class Person(var name: String, var tutorial : String)
    var person = Person("Anupam", "Kotlin")

    person.apply { this.tutorial = "Swift" }
    println("APPLY => "+person)

    data class Person1(var n: String, var t : String)
    var person1 = Person1("Anupam", "Kotlin")

    person1.apply { t = "Swift" }
    println(person1)

    person1.also { it.t = "Kotlin language" }
    println(person1)

    // output
    // Person1(n=Anupam, t=Swift)
    // Person1(n=Anupam, t=Kotlin)

}

fun withFunctionExample(){

    /*
    * Like apply, with is used to change instance properties without the need to call dot operator over the reference every time.
    * */

    data class Person(var name: String, var tutorial : String)
    var person = Person("Anupam", "Kotlin")

    with(person)
    {
        name = "No Name"
        tutorial = "Kotlin tutorials"
    }
    println(person)


}

