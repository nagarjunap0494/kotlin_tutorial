package accion.com.kotlintutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //main()
        //array()
        //collection()
        //extensionFunction()
        //inheritanceKotlin()
        dataClass()
    }

    /*
    * Switch Example
    * */
    private fun main() {
        // Display string.
        var name: String = "My Name is Naga"
        val size: Int = 3
        println("Print name $name")

        //Switch statement example.
        var value = when (size) {
            0 -> "One";
            1 -> "Two";
            2 -> "Three";
            3 -> "Four";
            else -> "other then this value"
        }
        println("Print Value is $value")
    }

    /*
    * Array or Range
    * */
    private fun array() {
        //for loop
        var loop = 1..10
        // Step is used to display the range between the numbers.
        for (a in loop step 3) {
            println("print loop values $a");
        }

        // If you want to display array values from top to low we need to user down to function.
        var uptodown = 10 downTo 1

        for (a in uptodown) {
            println("print upto down values $a")
        }
    }

    /*
    * Collection example
    * */
    private fun collection() {
        var numbers = listOf(1, 2, 3, 4, 5)

        for ((i, e) in numbers.withIndex()) {
            println("Print Index number is $i and value $e")
        }
        // i is represent of index
        // e represent of Value

        //TreeMap Example
        var mapdata = TreeMap<String, Int>()
        mapdata["Naga"] = 494
        mapdata["Yash"] = 949

        for ((name, age) in mapdata) {
            println("Print mapdata value $name, $age")
        }
    }

    /*
        1)  plus function is the extension function.
        2)  with out creating direct function we create extension function like plus function.
        3) Kotlin provides infix notation with which we can call a function with the
        class object without using a dot and parentheses across the parameter.
        */
    private fun extensionFunction() {
        var a1 = Allian()
        a1.skills = "JAVA"
        a1.show()

        var a2 = Allian()
        a2.skills = "KOTLIN"
        a2.show()

        var a3 = a1 + a2
        a3.show()
    }

    operator infix fun Allian.plus(allian: Allian): Allian {
        var newAllian = Allian()
        newAllian.skills = this.skills + " " + allian.skills
        return newAllian
    }

    private fun inheritanceKotlin() {
        var animal = Animal()
        animal.think()
    }

    /*
    * 1. Every class needs a string.
    * 2. Equals and hash code
    * 3. If you want to copy from one object to another we need to declare respect class as a
    * */
    fun dataClass() {
        var data1 = Laptop("Dell", 1234)
        var data2 = Laptop("Mac",4321)

        println("Print ${data1.equals(data2)}")

        println("print $data1")

        var data3 = data1.copy(price = 3456)

        println("print data3 $data3")
    }

    data class Laptop(val brand: String, val price: Int) {
        fun show(){
            println("Print My Awesome laptop...")
        }
    }
}