package accion.com.kotlintutorial

 open class Human {
    open fun think() {
        println("Print Human Class")
    }
}

class Animal : Human() {

    // By default in kotlin every class and functions are final if we want
    // to access we can defined OPEN keyword.
    override fun think(){
        println("print Animal Class")
    }
}