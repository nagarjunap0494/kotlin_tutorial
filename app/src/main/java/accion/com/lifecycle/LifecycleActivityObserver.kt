package accion.com.lifecycle

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/*
* This class we will learn Lifecycle of LifecycleObserver.
* */
class LifecycleActivityObserver : LifecycleObserver {

    val TAG : String = this.javaClass.simpleName

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreateEvent(){
        Log.i(TAG, "Owner_Observe ON_CREATE")
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(){
        Log.i(TAG, "Owner_Observe ON_START")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume(){
        Log.i(TAG, "Owner_Observe ON_RESUME")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause(){
        Log.i(TAG, "Owner_Observe ON_PAUSE")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop(){
        Log.i(TAG, "Owner_Observe ON_STOP")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy(){
        Log.i(TAG, "Owner_Observe ON_DESTROY")
    }

    /*@OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    fun onAny(){
        Log.i(TAG, "Owner_Observe ON_ANY")
    }*/
}