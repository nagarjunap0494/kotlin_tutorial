package accion.com.lifecycle

import accion.com.kotlintutorial.R
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_lifecycle.*

/*
* This class we will learn Activity life cycle.
* */
class LifecycleActivity : AppCompatActivity() {
    val TAG: String = this.javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lifecycle)

        Log.i(TAG, "Owner ON_CREATE")
        lifecycle.addObserver(LifecycleActivityObserver())

        button.setOnClickListener {
            Log.i(TAG, "ON CLICK")
        }
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, "Owner ON_START")
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "Owner ON_RESUME")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i(TAG, "Owner ON_RESTART")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "Owner ON_RESTART")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "Owner ON_STOP")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "Owner ON_DESTROYED")
    }
}